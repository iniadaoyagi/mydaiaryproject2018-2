from django.db import models

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=256)
    profile = models.TextField(default="")
    birthday =  models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name

class Diary(models.Model):
    title = models.CharField(max_length=256)
    # detail = models.TextField(default="")
    # created = models.DateTimeField(auto_now=True)
    # user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    # def __str__(self):
    #     return self.title
