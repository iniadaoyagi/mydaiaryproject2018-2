from django.shortcuts import render
from diaries.models import Diary

# Create your views here.
def top(request):
    """トップ画面"""
    return render(request, 'diaries/top.html')

def index(request):
    """日記一覧画面"""
    diaries = Diary.objects.all()
    data_dictionary = {'diaries': diaries}
    return render(request, 'diaries/index.html',  data_dictionary)

def create(request):
    """日記登録画面"""
    return render(request, 'diaries/create.html')

def detail(request, diary_id):
    """日記詳細画面"""
    diary = Diary.objects.get(id=diary_id)
    data_dictionary = {'diary': diary}
    return render(request, 'diaries/detail.html',  data_dictionary)